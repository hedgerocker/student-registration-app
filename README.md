# README #

Projekt zaliczeniowy na studia - formularz rejestracyjny.

### Jak skonfigurować ###

* Projekt wykonany przy użyciu Javy 8, Maven'a 3 oraz serwera Wildfly 8.2
* Konfiguracja:
** Datasource na serwerze wildfly należy skonfigurować zgodnie z [artykułem](http://wildfly.org/news/2014/02/06/GlassFish-to-WildFly-migration/). Zapamiętać zdefiniowaną jndi-name:  
** Stworzyć bazę danych z którą będziemy się łączyć (skrypt .sql w module student-ear/db)
** W pliku persistence.xml (student-domain) wpisać jta-data-source zgodnie z jndi-name.

### Jak uruchomić ###

* na module parent uruchamiamy komendę 
```
#!java

mvn clean install
```
* uruchamiamy aplikację ear na serwerze wildfly.
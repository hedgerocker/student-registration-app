package com.misiugas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the student database table.
 *
 */
@Entity
@NamedQuery(name="Student.findAll", query="SELECT s FROM Student s")
public class Student implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 3455043726167705310L;

	@Override
	public String toString() {
		return "Student [id=" + id + ", creationDate=" + creationDate
				+ ", imie=" + name + ", nazwisko=" + surname + "]";
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="creation_time")
	private Date creationDate;

	private String name;

	private String surname;

	private String birthdate;

	@Column(name="finished_schools")
	private String finishedSchools;

	public Student() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getFinishedSchools() {
		return finishedSchools;
	}

	public void setFinishedSchools(String finishedSchools) {
		this.finishedSchools = finishedSchools;
	}



}
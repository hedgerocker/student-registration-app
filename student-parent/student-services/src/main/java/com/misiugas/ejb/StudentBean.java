package com.misiugas.ejb;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;



@Stateless(mappedName = "StudentBean")
public class StudentBean {

	private Logger logger = Logger.getLogger("STUDENT-EJB");

	@PersistenceContext(unitName = "app-pu")
	EntityManager entityManager;

	public StudentBean() {
		logger.info("Student bean created...");
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean persist(Object o) {
		try {
			logger.info("Saving object: " + o);
			entityManager.persist(o);
			logger.info("Object saved: " + o);
			return true;
		} catch (Exception e) {
			logger.severe("StudentBean::persist: Error writing to DB: " + e);
			logger.severe("" + e.fillInStackTrace());
			return false;
		}
	}


}

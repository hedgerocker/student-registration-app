package com.misiugas.service.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.misiugas.Student;
import com.misiugas.ejb.StudentBean;
import com.misiugas.service.StudentLookupService;


@ManagedBean(eager = true)
@ApplicationScoped
public class StudentLookupDatabaseBean implements StudentLookupService, Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -7127394969378327680L;

	private Logger logger = Logger.getLogger("STUDENT_SERVICE");

	@EJB(name = "StudentBean")
	private StudentBean studentBean;

	public StudentLookupDatabaseBean() {
		logger.info("StudentLookupDatabaseBean created");
	}

	public StudentBean getStudentBean() {
		return studentBean;
	}

	public void setStudentBean(StudentBean studentBean) {
		this.studentBean = studentBean;
	}


	@Override
	public boolean persist(Student student) {
		return studentBean.persist(student);
	}




}

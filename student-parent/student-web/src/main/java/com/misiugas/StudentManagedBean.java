package com.misiugas;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.misiugas.Student;
import com.misiugas.service.StudentLookupService;


@ManagedBean
@SessionScoped
public class StudentManagedBean implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = 1893563693387274890L;

	protected Logger logger = Logger.getLogger("PGE-WEB");

	protected Student student;
	protected int id;
	protected String name;
	protected String surname;
	protected String birthdate;
	protected String finishedSchools;


	@ManagedProperty(value="#{studentLookupDatabaseBean}")
	private StudentLookupService studentLookupService;

	public StudentManagedBean() {
		super();
		logger.info("Student bean created...");
		student = new Student();
	}

	public StudentLookupService getStudentLookupService() {
		return studentLookupService;
	}

	public void setStudentLookupService(StudentLookupService studentLookupService) {
		this.studentLookupService = studentLookupService;
 	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		if (id == 0) {
			student = null;
			name = "";
			surname = "";
			birthdate = "";
			finishedSchools = "";
		}
	}



	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getFinishedSchools() {
		return finishedSchools;
	}

	public void setFinishedSchools(String finishedSchools) {
		this.finishedSchools = finishedSchools;
	}


	public String add() {
		FacesContext context = FacesContext.getCurrentInstance();
		this.student = new Student();
		this.student.setCreationDate(new Date());
		this.student.setName(name);
		this.student.setSurname(surname);
		this.student.setBirthdate(birthdate);
		this.student.setFinishedSchools(finishedSchools);
		if (this.studentLookupService.persist(this.student)) {
			context.addMessage(null, new FacesMessage("Dodano nowego studenta."));
		}
		else {
			context.addMessage(null, new FacesMessage("Błąd podczas dodania studenta."));
			return null;
		}
		return "page-b";//"edit.xhtml?id=" + news.getId();
	}



}
